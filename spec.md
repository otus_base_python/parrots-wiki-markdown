Ссылки на различные источники используемые в ходе проекта:

## App

<details>
<summary>PostgreSQL</summary>

- [PG Env Vars](https://www.postgresql.org/docs/9.1/libpq-envars.html)
- [PG image dockerhub](https://hub.docker.com/_/postgres)
- [PG create user,db,acces](https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e)

</details>

<details>
<summary>Production Django</summary>

- [checklist](https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/)
- [Configuring Django Settings for Production](https://thinkster.io/tutorials/configuring-django-settings-for-production)

</details>

---

## Infra

<details>
<summary>SealedSecrets</summary>

- [own cert](https://github.com/bitnami-labs/sealed-secrets/blob/main/docs/bring-your-own-certificates.md)

</details>

<details>
<summary>Ingress/DNS/TLS</summary>

- [godaddy](godaddy.com)
- [CodeWorld! - GCP + godaddy](https://www.youtube.com/watch?v=G5ZlnehNHsc)
- [CertBot](https://certbot.eff.org/)
- [LE](https://letsencrypt.org/getting-started/)
---
- [GCE-GKE](https://kubernetes.github.io/ingress-nginx/deploy/#gce-gke)
- [GKE-NGINX](https://cloud.google.com/community/tutorials/nginx-ingress-gke)
- [Nginx helm](https://cloud.google.com/community/tutorials/nginx-ingress-gke)
- [ArtifactHUB](https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx)
---
- [Setting up ExternalDNS on GKE with nginx-ingress-controller](https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/nginx-ingress.md)
- [Kubernetes, ingress-nginx, cert-manager & external-dns](https://blog.atomist.com/kubernetes-ingress-nginx-cert-manager-external-dns/)

</details>


<details>
<summary>Helm</summary>

- [kylesykes/genericwebapp](https://github.com/kylesykes/genericwebapp/tree/master/Chart/genericwebapp)
- [Helm Getting Started](https://helm.sh/docs/chart_template_guide/getting_started/)
- [Helm from basics to advanced — part I](https://banzaicloud.com/blog/creating-helm-charts/)
- [Helm from basics to advanced — part II](https://banzaicloud.com/blog/creating-helm-charts-part-2/)
- [Use Named Templates Like Functions in Helm Charts](https://itnext.io/use-named-templates-like-functions-in-helm-charts-641fbcec38da)
- [bitnami - redis](https://artifacthub.io/packages/helm/bitnami/redis)

</details>

<details>
<summary>Other</summary>

- [k rename ctx](https://github.com/kubernetes/kubernetes/issues/45131)

</details>
